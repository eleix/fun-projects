# fun-projects
Small code projects for fun

All solutions found in this repository are created as solutions to coding excercises found on 
https://github.com/karan/Projects-Solutions


# SOLUTIONS LIST AND OS SUPPORT

1. Coin Flip Simulator (coinflip.cpp)  | OS Agnostic (Universal)

# Buy me a coffee 

Like my work and want to show your support?

Paypal: 
https://www.paypal.me/eleix

Bytecoin: 
21zNFVshuMMgYFTStmhuY8PbErr7yWNu7MX4edBoDvXjeZgFg7Kfp5CKA32X5SrJuDPBgXTsSmQRoAbCzcDvM2d2PfvNuR3
