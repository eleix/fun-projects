/*

Coinflip Simulator | Github Project
Francis Booth (Eleix) 2017
https://www.github.com/eleix

Input: User provides number of flips
Output: Outcome of coin flips
Error Handling: User must specify a non-negative number.

Copyright (C) 2017 Francis Booth

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/

#include<iomanip>
#include<iostream>
#include<string>

using namespace std;

// Prototypes

void greeting();
int coinflip(long long int numFlips, long long int numHeads, long long int numTails, long long int numRounds, long long int numTies);

// Main 

int main() {

	long long int numFlips = 0; // Set initial flip value
	long long int numRounds = 0; // Resets the round count value
	long long int numHeads = 0; // Resets the heads count value
	long long int numTails = 0; // Resets the tails count value
	long long int numTies = 0; // Resets the tie count
	bool valid = true; // Assume that the user input is valid before starting
	char choice; // Location to store the user's choice to re-run the simulation without restarting the entire program

	greeting(); // Show the program name and version number
loop:do {
	valid = true; // Reset valid back on after bad number of flips
	cout << "Please specify the number of flips you would like: ";
	cin >> numFlips; // User specifies number of flips
	cout << endl;
	if (numFlips <= 0) {

		valid = false;
		cout << numFlips << " is not a valid number for flips, please specify a value above 0." << endl << endl; 

	}

} while (!valid);

clock_t tStart = clock(); // Start the stopwatch for tracking how long it takes to complete

coinflip(numFlips, numHeads, numTails, numRounds, numTies); // Run the simulation

printf("Time taken: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);  // Show the user how long it took to run the simulation

	cout << endl;
	cout << "Would you like to flip more coins? (y/n): ";
		cin >> choice;
		cout << endl;
	switch (choice) {

	case 'y':
	case 'Y': goto loop; break;
	case 'n':
	case 'N': cout << "Goodbye!" << endl;  break;

	}
return 0;
}

// Functions

void greeting() {

	// Output program greeting to prompt

	cout << "************************" << endl;
	cout << "*  Coinflip Simulator  *" << endl;
	cout << "*   By: Francis Booth  *" << endl;
	cout << "*                 v1.5 *" << endl;
	cout << "************************" << endl;
	cout << endl << endl;
}

int coinflip(long long int numFlips, long long int numHeads, long long int numTails, long long int numRounds, long long int numTies) {

	// Perform the specified number of flips requested by the user

	for (long long int count = 0; count < numFlips; count++) {

		float roll = rand() % 9999 + 1; // Randomly pick between 1 and 10000. 
		bool error = false; // Determine that the program is still functioning properly

							// Determine the landing face of the coin 

		if (roll > 5000) {
			numHeads++;
		}
		else if (roll < 5000) {
			numTails++;
		}
		else if (roll == 5000) {
			numTies++;
		}
		else {
			cout << "Program Error: Returned roll was not in expected range." << endl;
			cout << "Roll got " << roll << endl;
			cout << "Please submit a bug report on Github and include all program output." << endl; error = true; return 1; // Set the exit code of coinflip to 1 and error true
		}

		numRounds++;
	}

	string result;

	if (numHeads > numTails) {

		result = "Heads";

	}
	else {
		result = "Tails";
	}
	if (numHeads == numTails) {
		result = "Draw";
	}
	if (numTies > numHeads && numTails) {
		result = "???"; // If you ever get this to be true I will be shocked...
	}

	cout << "Coin Flip Simulation Statistics" << endl;
	cout << "Heads: " << numHeads << endl;
	cout << "Tails: " << numTails << endl;
	cout << "Ties: " << numTies << endl;
	cout << "Number of Rounds: " << numRounds << endl;

	if (numRounds == 1) {

		cout << endl;
		cout << "Winner: " << result << endl;

	}
	else {

		// Calculate percentages

		if (numHeads == numRounds) { // Heads 100%
			int headsPercInt;
			headsPercInt = 100;
			cout << "Percent Heads: " << headsPercInt << '%' << endl;
		}
		else { // Calculate heads percent
			float headsPercFloat;  
			headsPercFloat = (float(numHeads) / float(numRounds)) * 100;
			cout << "Percent Heads: " << setprecision(2) << headsPercFloat << '%' << endl;
		}
		if (numTails == numRounds) { // Tails 100%
			int tailsPercInt;
			tailsPercInt = 100.00;
			cout << "Percent Tails: " << tailsPercInt << '%' << endl;
		}
		else { // Calculate tails percent
			float tailsPercFloat;
			tailsPercFloat = (float(numTails) / float(numRounds)) * 100;
			cout << "Percent Tails: " << setprecision(2) << tailsPercFloat << '%' << endl;
		}
		if (numTies == numRounds) { // Ties 100% (Impossibru!)
			int tiesPercInt;
			tiesPercInt = 100.00;
			cout << "Percent Ties: " << tiesPercInt << '%' << endl;
		}
		else {
			float tiesPercFloat; // Calculate ties percent (Should ALWAYS be the case)
			tiesPercFloat = (float(numTies) / float(numRounds)) * 100;
			cout << "Percent Ties: " << setprecision(5) << tiesPercFloat << '%' << endl;
		}

		cout << endl;
		cout << "Winner: " << result << endl; // Display the highest winning side

	}

	return 0;

}
